import numpy as np
import pandas as pd

def drop_last_incomplete_burst(velocity_data):
    pattern1 = velocity_data.index.get_level_values(1) == velocity_data.index.levels[1].max()
    pattern2 = velocity_data.index.get_level_values(2) == velocity_data.index.levels[2].max()
    if not any(pattern1 & pattern2):
        pattern = velocity_data.index.get_level_values(1) < velocity_data.index.levels[1].max()
        velocity_data = velocity_data.loc[pattern, :]
    return velocity_data

def upsample(data, original_data):
    data = data[:, None, :] * np.ones([
        data.shape[0],
        original_data.index.levels[2].size,
        data.shape[1]]
    )
    data = data.reshape(original_data.shape)
    return pd.DataFrame(
                data,
                index=original_data.index,
                columns=original_data.columns
            )

def get_relative_deviation(data):
    data = drop_last_incomplete_burst(data)
    resampler = data.resample('T', level=0)
    mean = resampler.mean().values
    std = resampler.std().values
    mean = upsample(mean, data)
    std = upsample(std, data)
    relative_deviation = (data - mean) / std
    return relative_deviation

def filter_adcp_data(data, threshold=2.5):
    return data[get_relative_deviation(data).abs() < threshold]

