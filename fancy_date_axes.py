import matplotlib.dates as dates
import matplotlib.pyplot as plt

def make_fancy_date_axis(ax=None, xaxis=True):
    if not ax:
        ax = plt.gca()
    if xaxis:
        axis = ax.xaxis
        ax.tick_params(axis='x', pad=20)
    else:
        axis = ax.yaxis
        ax.tick_params(axis='y', pad=20)
    axis.set_minor_locator(dates.DayLocator())
    axis.set_minor_formatter(dates.DateFormatter('%d'))
    axis.set_major_locator(dates.MonthLocator())
    axis.set_major_formatter(dates.DateFormatter('%b\n%Y'))

