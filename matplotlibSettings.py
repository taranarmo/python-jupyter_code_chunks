params = {'figure.figsize': (10,6),
          'figure.dpi': 120,
          'text.usetex': False,
          'font.family': 'serif',
          'font.weight': 'bold',
          'grid.alpha': .4,
          'mathtext.default': 'default',
          'axes.grid': False,
          'axes.formatter.limits': [-3,7],
          'axes.titlesize':'small',
          'axes.labelsize': 'medium',
          'lines.markersize': 8,
          'savefig.bbox': 'tight',
          'savefig.transparent': False,
          'xtick.labelsize':'medium',
          'ytick.labelsize':'medium',
         }
plt.rcParams.update(params)

