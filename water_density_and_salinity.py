def get_water_salinity(t, conductivity=None, s=None):
    if conductivity:
        s = conductivity*0.8167/1000
    return sum([8.221e-4,
                -3.87e-6*t,
                4.99e-8*t**2])*s
    
def get_water_density(t, conductivity=None, s=None):
    if conductivity or s:
        sigma = sigma(t, s) if s else sigma(t, conductivity)
    else:
        sigma = 0
    return sum([0.9998395,
                6.7914e-5*t,
                -9.0894e-6*t**2,
                1.0171e-7*t**3,
                -1.2846e-9*t**4,
                1.1592e-11*t**5,
                -5.0125e-14*t**6,
                sigma])*1e3

def get_DO2(in_value, T, to_mg_per_litre=True):
    return in_value/100 * sum([
        14.16,
        -0.393*T,
        0.00745*T**2,
        -0.0000395*T**3,
        -0.00000116*T**4,
        0.00000002*T**5,
        ]
)
