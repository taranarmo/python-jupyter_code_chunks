def get_solar_zenith_angle(datetime, coords=dict(lat='62:10', lon='33:10')):
    datetime = pd.to_datetime(datetime)
    place = ephem.Observer()
    place.lat, place.lon = coords['lat'], coords['lon']
    place.date = datetime
    sun = ephem.Sun()
    sun.compute(place)
    return np.degrees(sun.alt)
