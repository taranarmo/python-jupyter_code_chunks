import os
import fnmatch
import pandas as pd

def read_nortek_data_csv(directory_path=None, filename=None, meaned=False, coords='beam'):
    init_directory = os.getcwd()
    if directory_path:
        os.chdir(directory_path)
    if not filename: # if filename is not explicitly given, try to get it from .hdr file; works if there is only one deployment data files set in one directory
        hdr_file = fnmatch.filter(names=os.listdir(), pat='*.hdr')
        hdr_file = hdr_file[0]
        filename = hdr_file.split('.')[0]
    index_col = 0 if meaned else [0,1,2] # if velocities already meaned by bursts, files have simple index, in other case MultiIndex should be used
    aquadopp = {}
    for comp in [f'v{i}' for i in (1,2,3)]:
        aquadopp[comp] = pd.read_csv(f'{filename}_{coords}.{comp}.csv', index_col=index_col, header=0, parse_dates=True)
        aquadopp[comp].columns = aquadopp[comp].columns.astype(float)
    os.chdir(init_directory)
    aquadopp = pd.concat(aquadopp, axis=1)
    return aquadopp

