import numpy as np
import pandas as pd

get_closest = lambda sequence,number: min(sequence, key=lambda x:abs(x-number))

def sf(df, ref=None, comp=1, power=2, r_power=2/3):
    idx = pd.IndexSlice
    if isinstance(df.columns,pd.MultiIndex):
        cells = df.columns.levels[-1]
    else:
        cells = df.columns
    if not ref:
        ref = get_closest(cells, np.median(cells))
    if ref not in df.columns.levels[-1].values:
        ref = get_closest(number=ref, sequence=df.columns.levels[-1].values)
    if isinstance(comp,int):
        comp = 'v'+str(comp)
    df = df.loc[:,idx[comp,:]]
    series = df.loc[:,idx[comp,ref]]
    ret_value = (df.sub(series,axis=0))**power
    if r_power == 1:
        ret_value.columns = ret_value.columns.get_level_values(1) - ref
    else:
        ret_value.columns = np.abs(ret_value.columns.get_level_values(1) - 
ref)**r_power
    return ret_value

def sf_meaned(df, ref=None, power=2, r_power=2/3):
    if isinstance(df.columns,pd.MultiIndex):
        cells = df.columns.levels[-1]
    else:
        cells = df.columns
    if not ref:
        ref = np.median(cells)
    sfs = []
    comps = df.columns.levels[0].values
    if ref not in df.columns.levels[-1].values:
        ref = get_closest(number=ref, sequence=df.columns.levels[-1].values)
    index = df.index
    for comp in comps:
        sfs.append(sf(df, ref=ref, comp=comp, power=power, r_power=r_power))
    ret_value = pd.DataFrame(np.mean(np.array([a.values for a in 
sfs]),axis=0),index=index,columns=sfs[0].columns)
    return ret_value

get_slope = lambda x, y: np.dot(x, y) / np.dot(x, x)

def evaluate_epsilon(r, sf_value, c=2.09, ret_err=False, printing=False, DLL=True):
    r = np.abs(np.array(r))
    pattern = np.argsort(r)
    r = r[pattern]
    sf_value = sf_value[pattern]
    amath = get_slope(r, sf_value)
    epsilon = (amath / (4/3))**(3/2)
    loops = int(len(r)/1.1)
    for i in range(loops):
        prevepsilon = epsilon
        r = r[:loops]
        sf_value = sf_value[:loops]
        amath = get_slope(r, sf_value)
        epsilon = (amath / c)**(3/2)
        uppersum = sum((sf_value-amath*r)**2)
        lowersum = sum((r)**2)
        err = np.sqrt(1 / loops * uppersum/lowersum) * 3/2 * epsilon ** (1/3)
        if printing:
            print(i, ' err ' ,err, ' abs(pe - e)', np.abs(epsilon - prevepsilon), 
len(r), r.max())
        if np.abs(epsilon - prevepsilon) < err or loops < 6:
            break
        loops = int(loops/1.1)
    return_list = []
    if ret_err:
        max_r_in_rd = np.max(r)/((1.6*10**(-6))**3/epsilon)**0.25
        return_value = {'e':epsilon,
                        'err':err,
                        'loops_left':loops,
                        'max_r':np.max(r),
                        'max_r_in_rd':max_r_in_rd,
                        'slope':amath,}
    else:
        return_value = epsilon
    return return_value

def epsilon(sf, ret_err=False, printing=False, DLL=True):
    c = 2.09 if DLL else 2.09*4/3
    if isinstance(sf,pd.DataFrame):
        sf = sf.mean()
    sf_value = sf.values
    r = sf.index
    return_value = evaluate_epsilon(r,
                                    sf_value,
                                    c=c,
                                    ret_err=ret_err,
                                    printing=printing,
                                    DLL=DLL)
    return return_value

def resample(data, resample_string='T'):
    data = data.resample(resample_string, level=0).mean()
    data.index = data.index.get_level_values(0)
    return data

def get_epsilon_array(
        current_velocity_dataframe,
        velocity_detrend='100T',
        averaging_str='100T',
        sf_func=sf_meaned,
        **sf_func_kwarg                      
):
    if isinstance(current_velocity_dataframe.index, pd.MultiIndex):
        current_velocity_dataframe = resample(current_velocity_dataframe)
    if velocity_detrend:
        current_velocity_dataframe = current_velocity_dataframe - \
                                     current_velocity_dataframe.rolling(velocity_detrend).mean()
    epsilon_array = {}
    for cell in current_velocity_dataframe.columns.levels[1]:
        sf_df = sf_func(current_velocity_dataframe, ref=cell, **sf_func_kwarg)
        sf_df = sf_df.rolling(averaging_str).mean()
        r = sf_df.columns.values
        epsilon_array[cell] = sf_df.apply(lambda x: evaluate_epsilon(r, x), axis=1, raw=True)
    return pd.DataFrame(epsilon_array)

